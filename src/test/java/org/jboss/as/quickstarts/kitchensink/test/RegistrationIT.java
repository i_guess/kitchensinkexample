/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jboss.as.quickstarts.kitchensink.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.as.quickstarts.kitchensink.data.ContestListProducer;
import org.jboss.as.quickstarts.kitchensink.data.MemberListProducer;
import org.jboss.as.quickstarts.kitchensink.model.Contest;
import org.jboss.as.quickstarts.kitchensink.model.Member;
import org.jboss.as.quickstarts.kitchensink.model.Team;
import org.jboss.as.quickstarts.kitchensink.model.TeamState;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaContestRepository;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaMemberRepository;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaTeamRepository;
import org.jboss.as.quickstarts.kitchensink.service.ContestRegistration;
import org.jboss.as.quickstarts.kitchensink.service.MemberRegistration;
import org.jboss.as.quickstarts.kitchensink.service.TeamRegistration;
import org.jboss.as.quickstarts.kitchensink.util.EntityManagerProducer;
import org.jboss.as.quickstarts.kitchensink.util.Resources;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class RegistrationIT {
    @Deployment
    public static Archive<?> createTestArchive() {

        File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve().withTransitivity().asFile();
        String deltaSpikeProps = "globalAlternatives.org.apache.deltaspike.jpa.spi.transaction.TransactionStrategy="
                + "org.apache.deltaspike.jpa.impl.transaction.BeanManagedUserTransactionStrategy";
        return ShrinkWrap.create(WebArchive.class, "test.war")
            .addClasses(Member.class, MemberRegistration.class, Resources.class, EntityManagerProducer.class, DeltaMemberRepository.class, 
                        Contest.class, ContestRegistration.class, DeltaContestRepository.class, 
                        DeltaTeamRepository.class, Team.class, TeamState.class, TeamRegistration.class,
                        ContestListProducer.class, MemberListProducer.class)
            .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
            .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
            .addAsWebInfResource(new StringAsset(deltaSpikeProps), "classes/META-INF/apache-deltaspike.properties").addAsLibraries(files)
            // Deploy our test datasource
            .addAsWebInfResource("test-ds.xml");
    }
    
    @Inject
    MemberRegistration memberRegistration;

    @Inject
    ContestRegistration contestRegistration;
    
    @Inject
    TeamRegistration teamRegistration;
    
    @Inject
    private DeltaMemberRepository dmr;
    
    @Inject
    private DeltaContestRepository dcr;
    
    @Inject
    private DeltaTeamRepository dtr;
    
    @Inject
    Logger log;
    
    private List<Member> uniqueMembersYoungerThan24;
    
    @After
    public void tearDown() {
        for (Team t : dtr.findAll()) {
            dtr.attachAndRemove(t);
        }
        for (Contest c : dcr.findAll()) {
            dcr.attachAndRemove(c);
        }
        for (Member m : dmr.findAll()) {
            dmr.attachAndRemove(m);
        }
    }
    
    @Before
    public void setUp() throws Exception {
        uniqueMembersYoungerThan24 = new ArrayList<Member>();
        for (int i = 1; i < 11; i++) {
            Member newMember = new Member();
            char[] lastName = new char[i];
            Arrays.fill(lastName, 'A');
            newMember.setName("Jane " + new String(lastName));
            newMember.setEmail("jane" + i + "@mailinator.com");
            newMember.setPhoneNumber("2000000000" + i);
            newMember.setBirthDate(new GregorianCalendar(1998, Calendar.FEBRUARY, i).getTime());
            uniqueMembersYoungerThan24.add(newMember);
        }
    }
    
    @Test
    public void testRegister() throws Exception {
        Member newMember = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember);
        assertNotNull(newMember.getId());
        log.info(newMember.getName() + " was persisted with id " + newMember.getId());
    }
    
    @Test
    public void testUnRegister() throws Exception {
    	
    	//Register new member to unregister
    	Member newMember = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember);
        assertNotNull(newMember.getId());
        
        //Unregister new member
        memberRegistration.remove(newMember);
        
    }
    
    @Test
    public void testChangeRegistration() throws Exception{
    	
    	//Register new member to change registration of
    	Member newMember = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember);
        long id = newMember.getId();
        assertNotNull(id);
        
        
        //Change email on member
        newMember.setEmail("noLongerJane@nolongermailinator.com");
        memberRegistration.save(newMember);
        assertEquals("noLongerJane@nolongermailinator.com", dmr.findOptionalById(id).getEmail());
    }

    @Test
    public void testContestRegister() throws Exception {
        Member newMember = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember);
        assertNotNull(newMember.getId());
        
        Contest contest = new Contest();
        contest.setName("First round");
        contest.setContestDate(Date.valueOf("2017-12-26"));
        contest.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest.setCapacity(10);
        contest.getContestManagers().add(newMember);
        contestRegistration.save(contest);
        assertNotNull(contest.getId());
        assertNotNull(dcr.findOptionalById(contest.getId()));
        assertEquals(newMember.getId(), dcr.findOptionalById(contest.getId()).getContestManagers().get(0).getId());
        log.info(contest.getName() + " was persisted with id " + contest.getId());
        
        Contest contest2 = new Contest();
        contest2.setName("Second round");
        contestRegistration.save(contest2);
        contest.setNextRound(contest2);
        contest2.getPreliminaryRounds().add(contest);
        contestRegistration.save(contest);
        contestRegistration.save(contest2);
        assertEquals(contest.getId(), contest2.getPreliminaryRounds().get(0).getId());
        assertEquals(contest.getId(), dcr.findOptionalById(contest2.getId()).getPreliminaryRounds().get(0).getId());
        assertEquals(contest2.getId(), dcr.findOptionalById(contest.getId()).getNextRound().getId());
                
    }
    
    @Test
    public void teamRegistrationPositiveBR1() throws Exception {
        Member newMember1 = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember1);
        
        Member newMember2 = uniqueMembersYoungerThan24.get(1);
        memberRegistration.save(newMember2);
        
        Member newMember3 = uniqueMembersYoungerThan24.get(2);
        memberRegistration.save(newMember3);
        
        Member newMember4 = uniqueMembersYoungerThan24.get(3);
        memberRegistration.save(newMember4);
        
        Member newMember5 = uniqueMembersYoungerThan24.get(4);
        memberRegistration.save(newMember1);
        
        Member newMember6 = uniqueMembersYoungerThan24.get(5);
        memberRegistration.save(newMember2);
        
        Member newMember7 = uniqueMembersYoungerThan24.get(6);
        memberRegistration.save(newMember3);
        
        Member newMember8 = uniqueMembersYoungerThan24.get(7);
        memberRegistration.save(newMember4);
        
        Contest contest = new Contest();
        contest.setName("First round");
        contest.setContestDate(Date.valueOf("2017-12-26"));
        contest.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest.setCapacity(2);
        

        Team newTeam = new Team();
        newTeam.setCoach(newMember1);
        newTeam.getMembers().add(newMember2);
        newTeam.getMembers().add(newMember3);
        newTeam.getMembers().add(newMember4);
        newTeam.setName("Positive team");
        newTeam.setContest(contest);
        
        
        Team newTeam2 = new Team();
        newTeam2.setCoach(newMember5);
        newTeam2.getMembers().add(newMember6);
        newTeam2.getMembers().add(newMember7);
        newTeam2.getMembers().add(newMember8);
        newTeam2.setName("Another positive team");
        newTeam2.setContest(contest);
        
        assertTrue(teamRegistration.checkTeamEligibility(newTeam));
        teamRegistration.save(newTeam);
        contest.getTeams().add(newTeam);
        assertTrue(teamRegistration.checkTeamEligibility(newTeam2));
        
        assertNotNull(newTeam.getId());
        
    }
    
    @Test
    public void teamCanRegisterNegativeBR1() throws Exception {
        Member newMember1 = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember1);
        
        Member newMember2 = uniqueMembersYoungerThan24.get(1);
        memberRegistration.save(newMember2);
        
        Member newMember3 = uniqueMembersYoungerThan24.get(2);
        memberRegistration.save(newMember3);
        
        Member newMember4 = uniqueMembersYoungerThan24.get(3);
        memberRegistration.save(newMember4);
        
        Member newMember5 = uniqueMembersYoungerThan24.get(4);
        memberRegistration.save(newMember5);
        
        Member newMember6 = uniqueMembersYoungerThan24.get(5);
        memberRegistration.save(newMember6);
        
        Member newMember7 = uniqueMembersYoungerThan24.get(6);
        memberRegistration.save(newMember7);
        
        Member newMember8 = uniqueMembersYoungerThan24.get(7);
        memberRegistration.save(newMember8);
        
        
        Contest contest = new Contest();
        contest.setName("First round");
        contest.setContestDate(Date.valueOf("2017-12-26"));
        contest.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest.setCapacity(2);
        
        Team newTeam = new Team();
        newTeam.setCoach(newMember1);
        newTeam.getMembers().add(newMember2);
        newTeam.getMembers().add(newMember3);
        newTeam.getMembers().add(newMember4);
        newTeam.setName("Positive team");
        newTeam.setContest(contest);
        
        Team newTeam2 = new Team();
        newTeam2.setCoach(newMember5);
        newTeam2.getMembers().add(newMember6);
        newTeam2.getMembers().add(newMember7);
        newTeam2.getMembers().add(newMember8);
        newTeam2.setName(newTeam.getName());
        newTeam2.setContest(contest);
        
        assertTrue(teamRegistration.checkTeamEligibility(newTeam));
        
        teamRegistration.save(newTeam2);
        contest.getTeams().add(newTeam2);

        assertFalse(teamRegistration.checkTeamEligibility(newTeam));     
        
        newTeam2.setName("not same as team 1");
        assertTrue(teamRegistration.checkTeamEligibility(newTeam));
        
        newTeam.setCoach(null);
        assertFalse(teamRegistration.checkTeamEligibility(newTeam));
        
        newTeam.setCoach(newMember1);
        assertTrue(teamRegistration.checkTeamEligibility(newTeam));
        
        newTeam.getMembers().remove(newMember2);
        assertFalse(teamRegistration.checkTeamEligibility(newTeam));
        
        newTeam.getMembers().add(newMember5);
        assertFalse(teamRegistration.checkTeamEligibility(newTeam));
        
        newTeam.getMembers().remove(newMember5);
        newTeam.getMembers().add(newMember6);
        assertFalse(teamRegistration.checkTeamEligibility(newTeam));
        
        newTeam.getMembers().remove(newMember6);
        newTeam.getMembers().add(newMember2);
        assertTrue(teamRegistration.checkTeamEligibility(newTeam));
        
        newMember2.setBirthDate(Date.valueOf("1992-10-31"));
        assertFalse(teamRegistration.checkTeamEligibility(newTeam));
        
    }
    
    @Test
    public void testContestManagers() throws Exception{
    	Contest contest = new Contest();
        contest.setName("First round");
        contest.setContestDate(Date.valueOf("2017-12-26"));
        contest.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest.setCapacity(2);
        
        
        Member newMember1 = new Member();
        newMember1.setName("Jane Qqqq");
        newMember1.setEmail("jane@mailinator.com");
        newMember1.setPhoneNumber("2125551234");
        newMember1.setBirthDate(new GregorianCalendar(1998, Calendar.FEBRUARY, 11).getTime());
        newMember1.getManagedContests().add(contest);
        memberRegistration.save(newMember1);
        
        
        for(Contest cont : newMember1.getManagedContests()) {
        	assertTrue(dcr.findOptionalById(cont.getId()).getContestManagers().isEmpty());
        	
        }
        
    }
    
    @Test
    public void testWritableContestNegativeBR2() throws Exception{
    	Contest contest = new Contest();
        contest.setName("First round");
        contest.setContestDate(Date.valueOf("2017-12-26"));
        contest.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest.setCapacity(2);
        contestRegistration.save(contest);
        
        Member newMember1 = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember1);
        
        Member newMember2 = uniqueMembersYoungerThan24.get(1);
        memberRegistration.save(newMember2);
        
        Member newMember3 = uniqueMembersYoungerThan24.get(2);
        memberRegistration.save(newMember3);
        
        Member newMember4 = uniqueMembersYoungerThan24.get(3);
        memberRegistration.save(newMember4);
        
        Team newTeam = new Team();
        newTeam.setCoach(newMember1);
        newTeam.getMembers().add(newMember2);
        newTeam.getMembers().add(newMember3);
        newTeam.getMembers().add(newMember4);
        newTeam.setName("Positive team");
        newTeam.setContest(contest);
        
        contest.getTeams().add(newTeam);
        
        contest.setRegistrationAllowed(false);
        contestRegistration.save(contest);
        
        assertFalse(contestRegistration.checkCanEdit(contest));
        
        assertFalse(teamRegistration.checkCanEdit(newTeam));

    }
    
    @Test
    public void testPromotionBR3() throws Exception {
    	
        Member newMember1 = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember1);
        
        Member newMember2 = uniqueMembersYoungerThan24.get(1);
        memberRegistration.save(newMember2);
        
        Member newMember3 = uniqueMembersYoungerThan24.get(2);
        memberRegistration.save(newMember3);
        
        Member newMember4 = uniqueMembersYoungerThan24.get(3);
        memberRegistration.save(newMember4);
    	
        Contest contest = new Contest();
        contest.setName("First rounssssdw");
        contest.setContestDate(Date.valueOf("2017-12-26"));
        contest.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest.setCapacity(2);
        
        Contest contest2 = new Contest();
        contest2.setName("SECOND! rounssssdw");
        contest2.setContestDate(Date.valueOf("2017-12-26"));
        contest2.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest2.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest2.setCapacity(2);
        contest2.getPreliminaryRounds().add(contest); 
        contest.setNextRound(contest2);
        contestRegistration.save(contest2);
        
        Team newTeam = new Team();
        newTeam.setCoach(newMember1);
        newTeam.getMembers().add(newMember2);
        newTeam.getMembers().add(newMember3);
        newTeam.getMembers().add(newMember4);
        newTeam.setName("Positive team");
        newTeam.setContest(contest);
        teamRegistration.save(newTeam);
        newTeam.setRank(1);
        
        assertTrue(teamRegistration.checkCanPromote(newTeam));
        
    }
    
    @Test
    public void testPromotionNegativeBR3() throws Exception {
    	
        Member newMember1 = uniqueMembersYoungerThan24.get(0);
        memberRegistration.save(newMember1);
        
        Member newMember2 = uniqueMembersYoungerThan24.get(1);
        memberRegistration.save(newMember2);
        
        Member newMember3 = uniqueMembersYoungerThan24.get(2);
        memberRegistration.save(newMember3);
        
        Member newMember4 = uniqueMembersYoungerThan24.get(3);
        memberRegistration.save(newMember4);
    	
        Contest contest = new Contest();
        contest.setName("First rounssssdw");
        contest.setContestDate(Date.valueOf("2017-12-26"));
        contest.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest.setCapacity(2);
        
        Contest contest2 = new Contest();
        contest2.setName("SECOND! rounssssdw");
        contest2.setContestDate(Date.valueOf("2017-12-26"));
        contest2.setRegistrationFrom(Date.valueOf("2017-12-01"));
        contest2.setRegistrationTo(Date.valueOf("2017-12-10"));
        contest2.setCapacity(0);
        contest2.getPreliminaryRounds().add(contest); 
        contest.setNextRound(contest2);
        contestRegistration.save(contest2);
        
        Team newTeam = new Team();
        newTeam.setCoach(newMember1);
        newTeam.getMembers().add(newMember2);
        newTeam.getMembers().add(newMember3);
        newTeam.getMembers().add(newMember4);
        newTeam.setName("Positive team");
        newTeam.setContest(contest);
        teamRegistration.save(newTeam);
        newTeam.setRank(1);
        
        
        assertFalse(teamRegistration.checkCanPromote(newTeam));
        
        contest2.setCapacity(1);
        assertTrue(teamRegistration.checkCanPromote(newTeam));
        newTeam.setRank(42);
        
        assertFalse(teamRegistration.checkCanPromote(newTeam));
    }
}
