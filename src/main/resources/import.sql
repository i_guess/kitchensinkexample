--
-- JBoss, Home of Professional Open Source
-- Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
-- contributors by the @authors tag. See the copyright.txt in the
-- distribution for a full listing of individual contributors.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- http://www.apache.org/licenses/LICENSE-2.0
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- You can use this file to load seed data into the database using SQL statements
insert into member (birthDate, name, email, phone_number) values ('2017-12-25', 'John Smith', 'john.smith@mailinator.com', '2125551212')
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithh', 'john.smith2@mailinator.com', '2125551214')
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithhq', 'john.smith3@mailinator.com', '2125551215') 
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithhw', 'john.smith4@mailinator.com', '2125551216') 
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithhe', 'john.smith5@mailinator.com', '2125551217') 
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithhr', 'john.smith6@mailinator.com', '2125551218')
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smiths', 'john.smith7@mailinator.com', '2125551219')
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithhg', 'john.smith8@mailinator.com', '2125551211') 
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithhh', 'john.smith9@mailinator.com', '21255512456') 
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithhj', 'john.smith10@mailinator.com', '2125551288') 
insert into member (birthDate, name, email, phone_number) values ('2017-12-27', 'John Smithhk', 'john.smith11@mailinator.com', '2125551207')
insert into contest (capacity, contestDate, name, registrationallowed, registrationfrom, registrationto) values (10, '2017-12-25', 'TestContest', true, '2017-12-01', '2017-12-10') 
insert into contest (capacity, contestDate, name, registrationallowed, registrationfrom, registrationto) values (10, '2017-12-25', 'NotEditableTestContest', false, '2017-12-01', '2017-12-10')
insert into team (contest_id, name, state, rank) values (1, 'editable-team1', 'PENDING', 1)
insert into team (contest_id, name, state, rank) values (1, 'editable-team2', 'PENDING', 1)
insert into team (contest_id, name, state, rank) values (2, 'not-editable-team1', 'PENDING', 1)
insert into team (contest_id, name, state, rank) values (2, 'not-editable-team1', 'PENDING', 1)
