package org.jboss.as.quickstarts.kitchensink.controller.teamstatetopic;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * <p>
 * A simple Message Driven Bean that asynchronously receives and processes the messages that are sent to the topic.
 * </p>
 *
 * @author Serge Pagop (spagop@redhat.com)
 */
@MessageDriven(name = "TeamStateTopicMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "topic/TEAMSTATETopic"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
public class TeamStateTopicMDB implements MessageListener {
    
    @Inject
    Event<MapMessage> jmsEvent;

    @Inject
    private Logger log;
    
    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message rcvMessage) {
        MapMessage msg = null;
        try {
            if (rcvMessage instanceof MapMessage) {
                msg = (MapMessage) rcvMessage;
                log.info("State change to team: ID :" + msg.getLong("team_id") + " new state:" + msg.getString("state"));
                jmsEvent.fire(msg);
            } else {
            }
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }
}