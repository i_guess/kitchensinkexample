package org.jboss.as.quickstarts.kitchensink.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Contest {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int capacity;

    @Column
    @Temporal(TemporalType.DATE)
    private Date contestDate;

    @Column
    private Boolean registrationAllowed = true;

    @Column
    @Temporal(TemporalType.DATE)
    private Date registrationFrom;

    @Column
    @Temporal(TemporalType.DATE)
    private Date registrationTo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contest")
    private List<Team> teams = new ArrayList<Team>();

    @ManyToMany
    @JoinTable(name = "CONTEST_MANAGERS", joinColumns = { @JoinColumn(name = "CONTEST_ID") }, inverseJoinColumns = {
            @JoinColumn(name = "MEMBER_ID") })
    private List<Member> contestManagers = new ArrayList<Member>();

    @OneToMany(mappedBy="nextRound", cascade= { CascadeType.ALL })
    private List<Contest> preliminaryRounds = new ArrayList<Contest>();

    @ManyToOne(cascade=CascadeType.ALL)
    private Contest nextRound;
    
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Date getContestDate() {
        return contestDate;
    }

    public void setContestDate(Date date) {
        this.contestDate = date;
    }

    public Boolean getRegistrationAllowed() {
        return registrationAllowed;
    }

    public void setRegistrationAllowed(Boolean registrationAllowed) {
        this.registrationAllowed = registrationAllowed;
    }

    public Date getRegistrationFrom() {
        return registrationFrom;
    }

    public void setRegistrationFrom(Date registrationFrom) {
        this.registrationFrom = registrationFrom;
    }

    public Date getRegistrationTo() {
        return registrationTo;
    }

    public void setRegistrationTo(Date registrationTo) {
        this.registrationTo = registrationTo;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<Member> getContestManagers() {
        return contestManagers;
    }

    public void setContestManagers(List<Member> contestManagers) {
        this.contestManagers = contestManagers;
    }

    public List<Contest> getPreliminaryRounds() {
        return preliminaryRounds;
    }

    public void setPreliminaryRounds(List<Contest> preliminaryRounds) {
        this.preliminaryRounds = preliminaryRounds;
    }

    public Contest getNextRound() {
        return nextRound;
    }

    public void setNextRound(Contest nextRound) {
        this.nextRound = nextRound;
    }
}
