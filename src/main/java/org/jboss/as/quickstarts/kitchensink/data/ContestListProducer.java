package org.jboss.as.quickstarts.kitchensink.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.as.quickstarts.kitchensink.model.Contest;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaContestRepository;

@RequestScoped
public class ContestListProducer {

    @Inject
    private DeltaContestRepository deltaRepo;

    private List<Contest> contests;
    
    @Produces
    @Named
    public List<Contest> getContests() {
        return contests;
    }
    
    @PostConstruct
    public void retrieveAllContests() {
        contests = deltaRepo.findAll();
    }
}
