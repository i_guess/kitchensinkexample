package org.jboss.as.quickstarts.kitchensink.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.as.quickstarts.kitchensink.model.Contest;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaContestRepository;

@Stateless
public class ContestRegistration {
    
    @Inject
    private Logger log;

    @Inject
    private DeltaContestRepository dcr;

    @Inject
    private Event<Contest> contestEventSrc;

    public void save(Contest contest) throws Exception {
        log.info("Registering " + contest.getName());
        dcr.save(contest);
        contestEventSrc.fire(contest);
    }
    
    public void remove(Contest contest) throws Exception{
        log.info("Unregistering " + contest.getName());
        dcr.remove(contest);
        contestEventSrc.fire(contest);
    }
    
    public boolean checkCanEdit(Contest contest) {
    	if (contest.getId() == null) {
    		return true;
    	}
    	return dcr.findOptionalById(contest.getId()).getRegistrationAllowed();
    }
    
}
