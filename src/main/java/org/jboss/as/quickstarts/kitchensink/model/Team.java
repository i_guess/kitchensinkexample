package org.jboss.as.quickstarts.kitchensink.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

@Entity
public class Team {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column
    private int rank = 0;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TeamState state = TeamState.PENDING;

    @OneToOne
    private Team promotedFromTeam;

    @ManyToMany
    @JoinTable(name = "TEAM_MEMBERS", joinColumns = { @JoinColumn(name = "TEAM_ID") }, inverseJoinColumns = {
            @JoinColumn(name = "PERSON_ID") })
    @Size(max = 3, message = "maximum 3 team members")
    private List<Member> members = new ArrayList<Member>();

    @ManyToOne(cascade=CascadeType.ALL)
    private Member coach;

    @ManyToOne(cascade= CascadeType.ALL)
    private Contest contest;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public TeamState getState() {
        return state;
    }

    public void setState(TeamState state) {
        this.state = state;
    }

    public Team getPromotedFromTeam() {
        return promotedFromTeam;
    }

    public void setPromotedFromTeam(Team promotedFromTeam) {
        this.promotedFromTeam = promotedFromTeam;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public Member getCoach() {
        return coach;
    }

    public void setCoach(Member coach) {
        this.coach = coach;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

}
