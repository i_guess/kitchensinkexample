package org.jboss.as.quickstarts.kitchensink.controller.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.as.quickstarts.kitchensink.model.Member;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaMemberRepository;

@Named
@ViewScoped
public class MemberConverter implements Converter, Serializable {

	@Inject
	DeltaMemberRepository dmr;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if( value != null && value.trim().length() > 0) {
			Member member = dmr.findOptionalById(Long.valueOf(value));
			return member;
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
			return String.valueOf(((Member) value).getId());
		}
		else {
			return null;
		}
	}

}
