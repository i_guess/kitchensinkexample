package org.jboss.as.quickstarts.kitchensink.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.as.quickstarts.kitchensink.model.Contest;
import org.jboss.as.quickstarts.kitchensink.model.Team;
import org.jboss.as.quickstarts.kitchensink.model.TeamState;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaContestRepository;
import org.jboss.as.quickstarts.kitchensink.service.ContestRegistration;
import org.jboss.as.quickstarts.kitchensink.service.TeamRegistration;

@Named
@RequestScoped
public class ContestDetailController implements Serializable {
    
    public static final String BR2_MESSAGE = "BR2. team/contest changes are allowed only if contest flag indicates it is writtable";

    
    @Named
    private Long selected;

    @Named
    private Contest selectedContest;
    
    @Inject
    private FacesContext facesContext;

    @Inject
    private TeamRegistration teamRegistration;

    @Inject
    private ContestRegistration contestRegistration;

    @Inject
    private DeltaContestRepository dcr;
    
    @PostConstruct
    public void initContest() {
        selected = Long.parseLong(facesContext.getExternalContext().getRequestParameterMap().get("contest_id"));
        selectedContest = dcr.findOptionalById(selected);
    }
    
    public void saveTeam(Team team) throws Exception {
        if(!contestRegistration.checkCanEdit(team.getContest())) {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, BR2_MESSAGE, "Changes unsuccessful");
            facesContext.addMessage(null, m);
            return;
        }
        try {
            teamRegistration.save(team);
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved!", "Changes successful");
            facesContext.addMessage(null, m);
        } catch (Exception e) {
            String errorMessage = getRootErrorMessage(e);
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "Registration unsuccessful");
            facesContext.addMessage(null, m);
        }
    }
    
    public Boolean isEnabled(Long id) {
        Contest c = dcr.findOptionalById(id);
        if (c == null) {
            return false;
        }
        return contestRegistration.checkCanEdit(c);
    }
    
//    public void onStateChange(Team team) throws Exception {
//        saveTeam(team);
//    }
    
    private String getRootErrorMessage(Exception e) {
        // Default to general error message that registration failed.
        String errorMessage = "Registration failed. See server log for more information";
        if (e == null) {
            // This shouldn't happen, but return the default messages
            return errorMessage;
        }

        // Start with the exception and recurse to find the root cause
        Throwable t = e;
        while (t != null) {
            // Get the message from the Throwable class instance
            errorMessage = t.getLocalizedMessage();
            t = t.getCause();
        }
        // This is the root cause message
        return errorMessage;
    }

    public Contest getSelectedContest() {
        return selectedContest;
    }

    public void setSelectedContest(Contest selectedContest) {
        this.selectedContest = selectedContest;
    }
    
    public TeamState[] getPossibleStates() {
        TeamState[] states = {TeamState.PENDING, TeamState.ACCEPTED, TeamState.CANCELED };
        return states;
    }
}
