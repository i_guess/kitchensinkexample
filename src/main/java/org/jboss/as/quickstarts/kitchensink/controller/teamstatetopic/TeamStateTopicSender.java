package org.jboss.as.quickstarts.kitchensink.controller.teamstatetopic;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Topic;

///**
// * Definition of the two JMS destinations used by the quickstart
// * (one queue and one topic).
// */
@JMSDestinationDefinitions(
    value = {
        @JMSDestinationDefinition(
            name = "java:/topic/TEAMSTATETopic",
            interfaceName = "javax.jms.Topic",
            destinationName = "TeamStateTopicMDB"
        )
    }
)

@Stateless
public class TeamStateTopicSender {
    
    @Resource(lookup = "java:/topic/TEAMSTATETopic")
    private Topic topic;
    
    @Inject
    private JMSContext jmsContext;
    
    @Inject
    private Logger log;
    
    public void sendTeamStateUpdate(Long id, String state) {
        log.info("Sending team update " + id + " " + state);
        try {
            MapMessage mapMsg = jmsContext.createMapMessage();
            mapMsg.setLong("team_id", id);
            mapMsg.setString("state", state);
            jmsContext.createProducer().send(topic, mapMsg);
        } catch (JMSException e) {
            log.log(Level.SEVERE, "Error during send TeamState update occured", e);
        }
    }
}
