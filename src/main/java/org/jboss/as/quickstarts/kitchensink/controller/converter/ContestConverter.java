package org.jboss.as.quickstarts.kitchensink.controller.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.as.quickstarts.kitchensink.model.Contest;
import org.jboss.as.quickstarts.kitchensink.repositories.DeltaContestRepository;

@Named
@ViewScoped
public class ContestConverter implements Converter, Serializable{
	
	@Inject
	DeltaContestRepository dcr;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if( value != null && value.trim().length() > 0) {
			Contest contest = dcr.findOptionalById(Long.valueOf(value));
			return contest;
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
			return String.valueOf(((Contest) value).getId());
		}
		else {
			return null;
		}
	}
}
