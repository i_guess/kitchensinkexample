package org.jboss.as.quickstarts.kitchensink.repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.jboss.as.quickstarts.kitchensink.model.Member;

@Repository(forEntity= Member.class)
public interface DeltaMemberRepository extends EntityRepository<Member, Long>{
	
	Member findOptionalById(Long id);
	List<Member> findAll();
	List<Member> findAllOrderByNameAsc();
}
