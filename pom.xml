<?xml version="1.0" encoding="UTF-8"?>
<!-- JBoss, Home of Professional Open Source Copyright 2015, Red Hat, Inc. 
	and/or its affiliates, and individual contributors by the @authors tag. See 
	the copyright.txt in the distribution for a full listing of individual contributors. 
	Licensed under the Apache License, Version 2.0 (the "License"); you may not 
	use this file except in compliance with the License. You may obtain a copy 
	of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
	by applicable law or agreed to in writing, software distributed under the 
	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
	OF ANY KIND, either express or implied. See the License for the specific 
	language governing permissions and limitations under the License. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<!-- <parent> <groupId>org.wildfly.quickstarts</groupId> <artifactId>quickstart-parent</artifactId> 
		<version>11.0.0-SNAPSHOT</version> <relativePath>../pom.xml</relativePath> 
		</parent> -->
	<groupId>org.wildfly.quickstarts</groupId>
	<artifactId>kitchensink-pg</artifactId>
	<version>11.0.0-SNAPSHOT</version>

	<packaging>war</packaging>
	<name>kitchensink-pg</name>
	<description>A starter Java EE web application project for use in JBoss EAP, generated from the jboss-javaee6-webapp archetype</description>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<properties>
		<maven.compiler.target>1.8</maven.compiler.target>
		<maven.compiler.source>1.8</maven.compiler.source>
		<qs.name.prefix>WildFly Quickstart:</qs.name.prefix>
		<root.dir>${project.basedir}</root.dir>
		<jboss.home.name>WILDFLY_HOME</jboss.home.name>
		<product.name>WildFly</product.name>
		<!-- A base list of dependency and plug-in version used in the various 
			quick starts. -->
		<version.wildfly.maven.plugin>1.2.0.Beta1</version.wildfly.maven.plugin>

		<!-- other plug-in versions -->
		<version.com.mycyla.license>3.0</version.com.mycyla.license>

		<!-- Explicitly declaring the source encoding eliminates the following 
			message: [WARNING] Using platform encoding (UTF-8 actually) to copy filtered 
			resources, i.e. build is platform dependent! -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<version.spring.framework>4.3.9.RELEASE</version.spring.framework>
		<!-- EAP component version management BOM -->
		<version.server.bom>11.0.0.Beta3</version.server.bom>

		<version.jboss.spec.javaee.7.0>1.1.0.Final</version.jboss.spec.javaee.7.0>

		<version.jaxws-tools-maven-plugin>1.2.0.Final</version.jaxws-tools-maven-plugin>

		<!-- Other dependency versions -->
		<version.ro.isdc.wro4j>1.7.9</version.ro.isdc.wro4j>
		<httpclient.version>4.5.3</httpclient.version>
		<version.org.apache.commons.commons-lang3>3.5</version.org.apache.commons.commons-lang3>
		<version.h2db>1.4.193</version.h2db>
		<version.org.wildfly.checkstyle-config>1.0.5.Final</version.org.wildfly.checkstyle-config>
		<linkXRef>false</linkXRef>
	</properties>
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.wildfly.bom</groupId>
				<artifactId>wildfly-javaee7-with-tools</artifactId>
				<version>${version.server.bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<dependency>
				<groupId>org.jboss.spec</groupId>
				<artifactId>jboss-javaee-7.0</artifactId>
				<version>${version.jboss.spec.javaee.7.0}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<dependency>
				<groupId>org.apache.deltaspike.distribution</groupId>
				<artifactId>distributions-bom</artifactId>
				<version>1.8.0</version>
				<type>pom</type>
			</dependency>
			<dependency>
				<groupId>org.jboss.arquillian</groupId>
				<artifactId>arquillian-bom</artifactId>
				<version>1.1.8.Final</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
		</dependencies>
	</dependencyManagement>
	<dependencies>


		<!-- First declare the APIs we depend on and need for compilation. All 
			of them are provided by JBoss EAP -->

		<!-- Import the CDI API, we use provided scope as the API is included in 
			JBoss EAP -->
		<dependency>
			<groupId>javax.enterprise</groupId>
			<artifactId>cdi-api</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Needed for running tests (you may also use TestNG) -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Now we declare any tools needed -->

		<!-- Annotation processor to generate the JPA metamodel classes for typesafe 
			criteria queries -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-jpamodelgen</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Bean Validation Implementation Provides portable constraints such 
			as @Email Hibernate Validator is shipped in JBoss EAP -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator</artifactId>
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-api</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Annotation processor that raising compilation errors whenever constraint 
			annotations are incorrectly used. -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator-annotation-processor</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Import the JPA API, we use provided scope as the API is included in 
			JBoss EAP -->
		<dependency>
			<groupId>org.hibernate.javax.persistence</groupId>
			<artifactId>hibernate-jpa-2.1-api</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Optional, but highly recommended -->
		<!-- Arquillian allows you to test enterprise code such as EJBs and Transactional(JTA) 
			JPA from JUnit/TestNG -->
		<dependency>
			<groupId>org.jboss.arquillian.junit</groupId>
			<artifactId>arquillian-junit-container</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.arquillian.protocol</groupId>
			<artifactId>arquillian-protocol-servlet</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Import the Common Annotations API (JSR-250), we use provided scope 
			as the API is included in JBoss EAP -->
		<dependency>
			<groupId>org.jboss.spec.javax.annotation</groupId>
			<artifactId>jboss-annotations-api_1.2_spec</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Import the EJB API, we use provided scope as the API is included in 
			JBoss EAP -->
		<dependency>
			<groupId>org.jboss.spec.javax.ejb</groupId>
			<artifactId>jboss-ejb-api_3.2_spec</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Import the JSF API, we use provided scope as the API is included in 
			JBoss EAP -->
		<dependency>
			<groupId>org.jboss.spec.javax.faces</groupId>
			<artifactId>jboss-jsf-api_2.2_spec</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Import the JAX-RS API, we use provided scope as the API is included 
			in JBoss EAP -->
		<dependency>
			<groupId>org.jboss.spec.javax.ws.rs</groupId>
			<artifactId>jboss-jaxrs-api_2.0_spec</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.primefaces</groupId>
			<artifactId>primefaces</artifactId>
			<version>6.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.core</groupId>
			<artifactId>deltaspike-core-impl</artifactId>
			<version>1.8.0</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.modules</groupId>
			<artifactId>deltaspike-data-module-api</artifactId>
			<version>1.8.0</version>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.modules</groupId>
			<artifactId>deltaspike-data-module-impl</artifactId>
			<version>1.8.0</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.modules</groupId>
			<artifactId>deltaspike-partial-bean-module-api</artifactId>
			<version>1.8.0</version>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.modules</groupId>
			<artifactId>deltaspike-proxy-module-api</artifactId>
			<version>1.8.0</version>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.modules</groupId>
			<artifactId>deltaspike-proxy-module-impl-asm5</artifactId>
			<version>1.8.0</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.modules</groupId>
			<artifactId>deltaspike-jpa-module-api</artifactId>
			<version>1.8.0</version>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.modules</groupId>
			<artifactId>deltaspike-jpa-module-impl</artifactId>
			<version>1.8.0</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.modules</groupId>
			<artifactId>deltaspike-partial-bean-module-impl</artifactId>
			<version>1.8.0</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.deltaspike.core</groupId>
			<artifactId>deltaspike-core-api</artifactId>
			<version>1.8.0</version>
		</dependency>
		<dependency>
			<groupId>org.jboss.shrinkwrap.resolver</groupId>
			<artifactId>shrinkwrap-resolver-impl-maven</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.spec.javax.websocket</groupId>
			<artifactId>jboss-websocket-api_1.1_spec</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.spec.javax.jms</groupId>
			<artifactId>jboss-jms-api_2.0_spec</artifactId>
			<scope>provided</scope>
		</dependency>
		<!-- Import the Servlet API, we use provided scope as the API is included 
			in JBoss EAP -->
		<dependency>
			<groupId>org.jboss.spec.javax.servlet</groupId>
			<artifactId>jboss-servlet-api_3.1_spec</artifactId>
			<scope>provided</scope>
		</dependency>
	</dependencies>
	<build>
		<!-- Set the name of the WAR, used as the context root when the app is 
			deployed -->
		<finalName>${project.artifactId}</finalName>
	</build>

	<profiles>
		<profile>
			<!-- All the modules that require nothing but JBoss Enterprise Application 
				Platform or JBoss EAP -->
			<id>default</id>
			<activation>
				<activeByDefault>true</activeByDefault>
				<property>
					<name>default</name>
					<value>!disabled</value>
				</property>
			</activation>
		</profile>
		<profile>
			<!-- An optional Arquillian testing profile that executes tests in your 
				JBoss EAP instance. This profile will start a new JBoss EAP instance, and 
				execute the test, shutting it down when done. Run with: mvn clean verify 
				-Parq-managed -->
			<id>arq-managed</id>
			<dependencies>
				<dependency>
					<groupId>org.wildfly.arquillian</groupId>
					<artifactId>wildfly-arquillian-container-managed</artifactId>
					<scope>test</scope>
				</dependency>
			</dependencies>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-failsafe-plugin</artifactId>
						<version>2.20</version>
						<executions>
							<execution>
								<goals>
									<goal>integration-test</goal>
									<goal>verify</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>
			<!-- An optional Arquillian testing profile that executes tests in a remote 
				JBoss EAP instance. Run with: mvn clean verify -Parq-remote -->
			<id>arq-remote</id>
			<dependencies>
				<dependency>
					<groupId>org.wildfly.arquillian</groupId>
					<artifactId>wildfly-arquillian-container-remote</artifactId>
					<scope>test</scope>
				</dependency>
			</dependencies>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-failsafe-plugin</artifactId>
						<version>2.20</version>
						<executions>
							<execution>
								<goals>
									<goal>integration-test</goal>
									<goal>verify</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
